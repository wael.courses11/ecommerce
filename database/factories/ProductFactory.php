<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'name' => $this->faker->name(),
            'price' => $this->faker->randomFloat(2,1,5999),
            'image'=>$this->faker->imageUrl(),
            'type' => $this->faker->boolean(2) ? 'laptop' : 'phone',
        ];
    }
}
