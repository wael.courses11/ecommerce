<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\WishList;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {

        $products = Product::whereIn('id',function ($query){
            $query->select('product_id')->from('wishlist')->where('user_id',1);
        })->get();
        return Response()->json(['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($id)
    {
        $cart = WishList::create(
            [
                'user_id' => Auth::user()->id,
                'product_id' => $id,
            ]
        );
        return Response()->json(['cart' => $cart]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WishList  $wishList
     * @return \Illuminate\Http\Response
     */
    public function show(WishList $wishList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WishList  $wishList
     * @return \Illuminate\Http\Response
     */
    public function edit(WishList $wishList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WishList  $wishList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WishList $wishList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WishList  $wishList
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        try{
            $carts = WishList::where('user_id', '=', Auth::user()->id)->get();
            foreach ($carts as $cart)
                $cart->delete();
        }
        catch (Exception $exception){
            return Response()->json(['message' => 'error.']);
        }
        return Response()->json(['message' => 'deleted successfully.']);
    }
}
